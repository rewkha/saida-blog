<?php if(!empty($_GET['tag'])) { ?>
<h1>Posts Tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></h1>
<?php } else { ?>
<span class="big-icon big-blog"></span><h1>Our Blog</h1>
<?php } ?>

<?php
if (!Yii::app()->user->isGuest) {
    echo CHtml::link(Yii::t('app','Написать новый пост'), Yii::app()->createUrl('blog/create') , array('class'=>'post-button')); 
} ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
    'pager' => array(
        'header' => '',
        'htmlOptions' => array(
            'class' => 'post-pager'
        )
    ),
)); ?>
