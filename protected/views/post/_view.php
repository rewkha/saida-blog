<?php 
    $update_date = new DateTime($data->update_time); 
?>

<div class="post">
    <div class="post-before">
        <span>Last updated on <?php echo date_format($update_date, 'Y-m-d H:i:s');; ?></span>
        <?php  echo CHtml::link('<span class="icon"></span>', $data->url, array('title'=>'permalink')); ?> 
    </div>
    <div class="post-view">
        <div class="post-info">
            <div class="author">
                <img src='<?php echo Yii::app()->baseUrl."/images/avatar/".$data->author->getAvatar() ?>'>
            </div>
        </div>
        <div class="post-content">
            <div class="post-title">
                <?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
            </div>
            <div class="post-text">
                <?php
                        $this->beginWidget('CMarkdown', array('purifyOutput'=>false));
                        echo $data->content;
                        $this->endWidget();
                ?>
            </div>
        </div>
    </div>
    <div class="post-bottom">
        <div class="post-comments">
            <?php  
             if ($data->commentCount) {
                 echo CHtml::link("<span class='icon'></span> ({$data->commentCount})",$data->url.'#comments');
             } else {
                 echo CHtml::link("<span class='icon'></span>",$data->url.'#comments');
             } ?> 
        </div>
        <div class="tags">
            <?php if ($data->tagLinks) { 
                echo CHtml::tag('span', array('class'=>'icon'), null, true);
                echo implode(', ', $data->tagLinks);
            } ?>
        </div>
    </div>
</div>