<?php
$deleteJS = <<<DEL
$('.container').on('click','.time a.delete',function() {
	var th=$(this),
		container=th.closest('div.comment'),
		id=container.attr('id').slice(1);
        console.log(container);
	if(confirm('Are you sure you want to delete comment #'+id+'?')) {
		$.ajax({
			url:th.attr('href'),
			type:'POST'
		}).done(function(){container.slideUp()});
	}
	return false;
});
DEL;
Yii::app()->getClientScript()->registerScript('delete', $deleteJS);
?>

<?php foreach($comments as $comment): ?>
<?php $create_date = new DateTime($comment->create_time); ?>

<div class="comment" id="c<?php echo $comment->id; ?>">

	<?php echo CHtml::link("<span class='icon'></span>", $comment->getUrl($post), array(
		'class'=>'cid',
		'title'=>'Permalink to this comment',
	)); ?>

	<div class="author">
		<?php echo $comment->authorLink; ?> says:
	</div>

	<div class="time">
        <?php if (! Yii::app()->user->isGuest) {
            echo CHtml::link('Delete',array('comment/delete','id'=>$comment->id),array('class'=>'delete')); ?> |
            <?php } ?>
		<?php echo date_format($create_date, 'Y-m-d H:i:s'); ?>
	</div>

	<div class="content">
		<?php echo nl2br(CHtml::encode($comment->content)); ?>
	</div>

</div><!-- comment -->
<?php endforeach; ?>