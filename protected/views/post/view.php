<?php
$this->pageTitle=$model->title;
$update_date = new DateTime($model->update_time); 

?>

<div class="post">
    <div class="post-before">
        <span>Last updated on <?php echo date_format($update_date, 'Y-m-d H:i:s');; ?></span>
    </div>
    <div class="post-view">
        <div class="post-info">
            <div class="author">
                <img src='<?php echo Yii::app()->baseUrl."/images/avatar/".$model->author->getAvatar() ?>'>
            </div>
        </div>
        <div class="post-content">
            <div class="post-title">
                <?php echo CHtml::link(CHtml::encode($model->title), $model->url); ?>
            </div>
            <div class="post-text">
                <?php
                        $this->beginWidget('CMarkdown', array('purifyOutput'=>false));
                        echo $model->content;
                        $this->endWidget();
                ?>
            </div>
        </div>
    </div>
    <div class="post-bottom">
        <div class="tags">
            <?php if ($model->tagLinks) { 
                echo CHtml::tag('span', array('class'=>'icon'), null, true);
                echo implode(', ', $model->tagLinks);
            } ?>
        </div>
    </div>
</div>

<div id="comments">
	<?php if($model->commentCount>=1) { ?>
		<h3>
			<?php echo $model->commentCount>1 ? $model->commentCount . ' comments:' : 'One comment:'; ?>
		</h3>

		<?php $this->renderPartial('_comments',array(
			'post'=>$model,
			'comments'=>$model->comments,
		)); ?>
	<?php } ?>

	<h3>Leave a Comment</h3>

	<?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
		<div class="flash-success">
			<?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
		</div>
	<?php else: ?>
		<?php $this->renderPartial('/comment/_form',array(
			'model'=>$comment,
		)); ?>
	<?php endif; ?>

</div><!-- comments -->
