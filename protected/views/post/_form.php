<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>
    
	<?php echo CHtml::errorSummary($model); ?>

    <div class="row">
		<?php echo CHtml::activeDropDownList($model,'author_id', array(
            Yii::app()->user->id => 'От своего имени',
            3 => 'Общее авторство',
        ) ); ?>
	</div>
    
	<div class="row">
		<?php //echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>80,'maxlength'=>128, 'class'=>'input-xxlarge',  'placeholder'=>'Название поста')); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php // echo CHtml::activeTextArea($model,'content',array('rows'=>10, 'cols'=>70)); 
        $attribute='content';
           $this->widget('application.widgets.redactorjs.Redactor', array(
               'model' => $model, 
               'attribute' => $attribute,
               //'toolbar' => 'mini',
               'lang'=>'ru',
               'editorOptions' => array(
                   'fileUpload'=>Yii::app()->createUrl('blog/fileUpload',array(
                        //'attr'=>$attribute
                    )),
                    'fileUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                    'imageUpload'=>Yii::app()->createUrl('blog/imageUpload',array(
                       // 'attr'=>$attribute
                    )),
                    'imageGetJson'=>Yii::app()->createUrl('blog/imageList',array(
                       // 'attr'=>$attribute
                    )),
                    'imageUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'),
                   
                   ),
                )
           );
        ?>
        
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row tags-row">
		<?php //echo $form->labelEx($model,'tags'); ?>
        <div class="input-prepend">
            <span class="add-on">Tags</span>
            <?php $this->widget('CAutoComplete', array(
                'model'=>$model,
                'attribute'=>'tags',
                'url'=>array('suggestTags'),
                'multiple'=>true,
                'htmlOptions'=>array('size'=>50),
            )); ?>
        </div>
		<p class="hint">Пожалуйста, разделяйте теги запятыми.</p>
		<?php echo $form->error($model,'tags'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Опубликовать' : 'Сохранить', array('class'=>'buttn')); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->