<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form CActiveForm */
$this->pageTitle = Yii::t('app','Profile');
?>
<h4>Редактирование личных данных</h4>

<div class="change-pad">
    <?php echo CHtml::link(Yii::t('app','Изменить пароль'), Yii::app()->createUrl('admin/changePassword') , array('class'=>'btn btn-small btn-inverse')); ?>
    <?php echo CHtml::link(Yii::t('app','Изменить общий ватар'), Yii::app()->createUrl('admin/commonPhoto') , array('class'=>'btn btn-small btn-inverse')); ?>
    <?php echo CHtml::link(Yii::t('app','Удалить аватар'), Yii::app()->createUrl('admin/deleteAvatar') , array('class'=>'btn btn-small btn-danger')); ?>
</div>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'account-profile-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'pic'); ?>
        <div class="avatar">
            <div class="circle-70">
                <img src='<?php echo Yii::app()->baseUrl."/images/avatar/".$model->getAvatar() ?>'>
            </div>
        </div>
		<div class="secondary">только jpg файлы, не более 250 кб.</div>
		<?php echo $form->fileField($model,'pic'); ?>
		<?php echo $form->error($model,'pic'); ?>

	</div>
 






	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
