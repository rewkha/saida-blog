<?php $this->pageTitle = Yii::t('app','Управление галереями'); ?>

<h4>Управление галлерями</h4>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-block alert-' . $key . '"> <a href="#" class="close" data-dismiss="alert">&times;</a>' . $message . "</div>\n";
    }
?>
<?php echo CHtml::link(Yii::t('app','Создать новую'), Yii::app()->createUrl('admin/newGallery') , array('class'=>'btn btn-small btn-inverse')); ?>

<?php foreach ($galleries as $gallery) { 
    if (!empty($gallery->galleryPhotos)) {
        $image = '_' . $gallery->galleryPhotos[0]->id . '.jpg';
    } else {
        $image = '0.jpg';
    }
    ?>
<div class="gallery-wrapper">
    <a class="gallery-thumb" href ="<?php echo Yii::app()->createUrl('admin/editGallery', array('id'=>$gallery->id)); ?>">
        <img src="<?php echo Yii::app()->getBaseUrl(true) . '/gallery/'. $image  ; ?>">
        <span class="gallery-name"><?php echo $gallery->gallery_name; ?></span>
    </a>
</div>
    
<?php } ?>
