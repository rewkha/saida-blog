<h4>Управление постами в блоге</h4>
<?php echo CHtml::link(Yii::t('app','Написать новый пост'), Yii::app()->createUrl('blog/create') , array('class'=>'btn btn-small btn-inverse')); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'htmlOptions'=> array(
        'class'=>'table table-hover',
    ),
	'columns'=>array(
		array(
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->title), $data->url)'
		),
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
        array(
			'name'=>'update_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'class'=>'CButtonColumn',
            'viewButtonUrl'  =>'Yii::app()->createUrl("/blog/view", array("id" => $data->id))',
            'deleteButtonUrl'=>'Yii::app()->createUrl("/blog/delete", array("id" => $data->id))',
            'updateButtonUrl'=>'Yii::app()->createUrl("/blog/update", array("id" => $data->id))',
		),
	),
)); ?>
