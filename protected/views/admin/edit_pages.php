<?php
/* @var $this StaticPagesController */
/* @var $model StaticPages */
/* @var $form CActiveForm */

$this->pageTitle = Yii::t('app','Редактироание страницы') .': ' . $model->name;
?>

<ul class="nav nav-pills">
  <li <?php if ($current == 1) echo 'class="active"'; ?> >
      <a href="<?php echo Yii::app()->createUrl('admin/editPages', array('type'=>'1'));?>">Главная</a>
  </li>
  <li <?php if ($current == 3) echo 'class="active"'; ?> >
      <a href="<?php echo Yii::app()->createUrl('admin/editPages', array('type'=>'3'));?>">О нас</a>
  </li>
  <li <?php if ($current == 2) echo 'class="active"'; ?> >
      <a href="<?php echo Yii::app()->createUrl('admin/editPages', array('type'=>'2'));?>">Контакты</a>
  </li>
</ul>


<h2><?php echo $model->name; ?></h2>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'static-pages-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <?php if ($model->type == 2) { ?>
    <div class="row">  
        <?php echo CHtml::label(Yii::t('app','Email'), 'StaticPages_content'); ?>
        <?php echo $form->textField($model,'content'); ?>
        
        <?php echo $form->error($model,'content'); ?>
        <small style="color:grey;"> Введите валидный email, на него вам будут присылать то, что напишут в Contact Us</small>
    </div>
    <?php } else { ?>
    <div class="row">        
        <?php echo $form->labelEx($model,'content'); ?>
        <?php //echo $form->textField($model,'content'); 
           $attribute='content';
           $this->widget('application.widgets.redactorjs.Redactor', array(
               'model' => $model, 
               'attribute' => $attribute,
               //'toolbar' => 'mini',
               'lang'=>'ru',
               'editorOptions' => array(
                   'fileUpload'=>Yii::app()->createUrl('admin/fileUpload',array(
                        //'attr'=>$attribute
                    )),
                    'fileUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'
                    ),
                    'imageUpload'=>Yii::app()->createUrl('admin/imageUpload',array(
                       // 'attr'=>$attribute
                    )),
                    'imageGetJson'=>Yii::app()->createUrl('admin/imageList',array(
                       // 'attr'=>$attribute
                    )),
                    'imageUploadErrorCallback'=>new CJavaScriptExpression(
                        'function(obj,json) { alert(json.error); }'),
                   
                   ),
                )
           );
        ?>
        <?php echo $form->error($model,'content'); ?>
    </div>
    <?php } ?>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->