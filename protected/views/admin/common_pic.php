<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form CActiveForm */
$this->pageTitle = Yii::t('app','Common Photo');
?>
<h4>Редактирование общего фото</h4>

<div class="change-pad">
    <?php echo CHtml::link(Yii::t('app','Удалить аватар'), Yii::app()->createUrl('admin/deleteAvatar') , array('class'=>'btn btn-small btn-inverse')); ?>
</div>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'account-profile-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'pic'); ?>
        <div class="avatar">
            <div class="circle-70">
                <img src='<?php echo Yii::app()->baseUrl."/images/avatar/".$model->getAvatar() ?>'>
            </div>
        </div>
		<div class="secondary">только jpg файлы, не более 250 кб.</div>
		<?php echo $form->fileField($model,'pic'); ?>
		<?php echo $form->error($model,'pic'); ?>

	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
