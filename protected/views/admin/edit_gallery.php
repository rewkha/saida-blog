<h4>Редактирование галереи "<?php echo $gallery->gallery_name; ?>"</h4>

<?php
$this->widget('ext.galleryManager.GalleryManager', array(
    'gallery' => $gallery,
    'controllerRoute' => '/gallery', 
));
?>

<div class="form form-pad">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'edit-gallery-form',
)); ?>

    <?php echo $form->errorSummary($gallery); ?>

    <div class="row">
        <?php echo $form->labelEx($gallery,'gallery_name'); ?>
        <?php echo $form->textField($gallery,'gallery_name'); ?>
        <?php echo $form->error($gallery,'gallery_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($gallery,'gallery_desc'); ?>
        <?php echo $form->textArea($gallery,'gallery_desc'); ?>
        <?php echo $form->error($gallery,'gallery_desc'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить', array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>
    <div class="delete-pad">
<?php echo CHtml::button(
        Yii::t('app','Удалить галерею'), 
        array('submit' =>Yii::app()->createUrl('gallery/deleteGallery', array("id" => $gallery->id)),
        'confirm'=>'Вы уверены, что хотите удалить галерею? Загруженные фото пропадут. Все что было станет тленом.', 'name'=>'delete', 'class'=>'btn btn-small btn-danger')
    ); ?>
    </div>
</div><!-- form -->


