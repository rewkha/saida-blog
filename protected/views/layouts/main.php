<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
    <meta name="keywords" content="Vicoolya, Saida, photographer, photo, art, website, app, photograph, art-photographer, digital" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<img src="/images/y.jpg" id="bg" alt=""></img>
<?php Yii::app()->clientScript->registerScript('Script', "
            
$(window).load(function() {    
	var theWindow        = $(window),
	    bg              = $('#bg'),
	    aspectRatio      = bg.width() / bg.height();		    		
	function resizeBg() {
		
		if ( (theWindow.width() / theWindow.height()) < aspectRatio ) {
		    bg
		    	.removeClass()
		    	.addClass('bgheight');
		} else {
		    bg
		    	.removeClass()
		    	.addClass('bgwidth');
		}
					
	}
	                   			
	theWindow.resize(resizeBg).trigger('resize');

});"); ?>
<div>
<div class="sidebar-left">
    <a class="logo" href="<?php echo Yii::app()->getBaseUrl(true) ?>"></a>        
    <?php $this->renderPartial('//layouts/_menu'); ?>
    
    <div class="exp-menu">
        <a class="sitemap-btn" href="/sitemap" title="sitemap"><span class="icon"></span></a>
        <a class="admin-btn" href="/admin" title="admin"><span class="icon"></span></a>
        <?php if (!Yii::app()->user->isGuest) { ?>
             <a class="logout-btn" href="/admin/logout" title="logout"><span class="icon"></span></a>
        <?php } ?>
    </div>
</div>
    
<div class="container" id="page">
    <?php echo $content; ?>
</div><!-- page -->

</div>
</body>
</html>
