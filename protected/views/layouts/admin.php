<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="header">
    
    
    <?php if (!Yii::app()->user->isGuest) { ?>
    
    <? $this->widget('zii.widgets.CMenu',array(
			'htmlOptions'=> array(
                'class'=>"nav nav-tabs"
            ),
            'items'=>array(
                array(
                    'label'=>'Главная',
                    'url'=>array('/index'),
                    'itemOptions'  => array('class'=>'divider-vertical home-url'),
                ),
                array(
                    'label'=>'Профайл',
                    'url'=>array('/admin/profile'),
                    'itemOptions'  => array('class'=>'divider-vertical'),
                ),
				array(
                    'label'=>'Управление блогом',
                    'url'=>array('/admin/posts'),
                    'itemOptions'  => array('class'=>'divider-vertical'),
                ),
                array(
                    'label'=>'Галлереи',
                    'url'=>array('/admin/galleries'),
                    'itemOptions'  => array('class'=>'divider-vertical'),
                ),
                
                array(
                    'label' => 'Статичные страницы', 
                    'url'=>array('/admin/editPages'),
                    'itemOptions'  => array('class'=>'divider-vertical'),
                ),
                
                
//                array(
//                    'label' => 'Auth',
//                    'linkOptions' => array('class'=>'nav-header'),
//                ),
                array(
                    'label'=>'Выйти ('.Yii::app()->user->name.')',
                    'url'=>array('/admin/logout'),
                ),
			),
		)); 
    
//    array(
//                            'label'=>'First page',
//                            'url'=>array('/admin/editPages/type/1'),
//                        ),
//                        array(
//                            'label'=>'About',
//                            'url'=>array('/admin/editPages/type/3'),
//                        ),
//                        array(
//                            'label'=>'Contacts',
//                            'url'=>array('/admin/editPages/type/2'),
//                        ),
    }?>
</div>
    
<div class="container" id="page">
    <?php echo $content; ?>

<div class="clear"></div>
</div><!-- page -->

</body>
</html>
