<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
            'itemTemplate' => '{menu} <span class="icon"></span>',
            'linkLabelWrapper' => 'span',
            'linkLabelWrapperHtmlOptions' => array(
                'class'=>'text'
            ),
			'items'=>array(
                array(
                    'label'=>'Photo', 
                    'url'=>array('/photo'),
                    'itemOptions' => array(
                        'class'=>'photo-menu',
                    ),
                ),
                array(
                    'label'=>'Blog', 
                    'url'=>array('/blog'),
                    'itemOptions' => array(
                        'class'=>'blog-menu',
                    ),
                ),
                array(
                    'label'=>'About', 
                    'url'=>array('/site/about', 
                        //'view'=>'about'
                        ),
                    'itemOptions' => array(
                        'class'=>'about-menu',
                    ),
                ),
                array(
                    'label'=>'Contact',
                    'url'=>array('/site/contact'),
                    'itemOptions' => array(
                        'class'=>'contact-menu',
                    )
                    
                ),
			),
		)); ?>
</div>