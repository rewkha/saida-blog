
<span class="big-icon big-photo"></span><h1>Photo Gallery</h1>

<?php foreach ($galleries as $gallery) { 
    if (!empty($gallery->galleryPhotos)) {
        $image = '_' . $gallery->galleryPhotos[0]->id . '.jpg';
    } else {
        $image = '0.jpg';
    }
    ?>
<div class="gallery-wrapper">
    <a class="gallery-thumb" href ="<?php echo Yii::app()->createUrl('photo/gallery', array('id'=>$gallery->id)); ?>">
        <img src="<?php echo Yii::app()->getBaseUrl(true) . '/gallery/'. $image  ; ?>">
        <span class="gallery-name"><?php echo $gallery->gallery_name; ?></span>
    </a>
</div>
    
<?php } ?>
