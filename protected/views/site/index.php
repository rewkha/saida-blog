<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1><?php echo $title ?></h1>

<div class="static-content">
    <?php echo $content; ?>
</div>
