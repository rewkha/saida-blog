<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=> 'Vicoolya and Saida',
    //'sourceLanguage' => 'en_us',
    //'language' => 'ru',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'ext.galleryManager.models.*',
        
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
//		'gii'=>array(
//			'class'=>'system.gii.GiiModule',
//			'password'=>'123',
//			// If removed, Gii defaults to localhost only. Edit carefully to taste.
//			'ipFilters'=>array('127.0.0.1','192.168.182.1','::1'),
//		)
        'sitemap' => array(
            'class' => 'ext.sitemap.SitemapModule',     //or whatever the correct path is
            'actions' => array(
                'site/index',
                'site/contact',
                'site/about',
                array(
                    'route' => 'blog/index',
                ),
                array(
                    'route' => 'blog/view',
                    'params' => array( //specify action parameters
                        'model' => array(
                            'class' => 'Post',
                            'criteria' => array('condition' => 'create_time > NOW() - INTERVAL 30 DAY'),
                            'map' => array(
                                'id' => 'id',
                            ),
                        ),              
                    ),
                ),
                'photo/index',
                 array(
                    'route' => 'photo/gallery',
                    'params' => array( //specify action parameters
                        'model' => array(
                            'class' => 'Gallery',
                            //'criteria' => array('condition' => 'id > NOW() - INTERVAL 30 DAY')
                            'map' => array(
                                'id' => 'id',
                            ),
                        ),              
                    ),
                ),
            ),                    
            'absoluteUrls' => true,               
            'protectedControllers' => array('admin'),  
            'protectedActions' =>array('site/error'),   
            'priority' => '0.5',                        
            'changefreq' => 'daily',                                     
            'cacheId' => 'cache',                       
            'cachingDuration' => 3600,                       
        ),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
            'loginUrl'=>array('admin/login'),
			'allowAutoLogin'=>true,
		),
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            //'driver'=>'GD',
            // ImageMagick setup path
//            'params'=>array('directory'=>'D:/Program Files/ImageMagick-6.4.8-Q16'),
            //'params'=>array('directory'=>'/usr/local/bin'),
            
        ),
        
        
        
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
				'blog/<id:\d+>/<title:.*?>'=>'post/view',
               // 'blog/view/<id:\d+>'=>'post/view',
                'tag/<tag:.*?>'=>'post/index',
                'blog/update/<id:\d+>'=>'post/update',
                'blog/<action:\w+>'=>'post/<action>',
                'blog'=>'post/index',
                'index' => 'site/index',
//                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
//				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                'sitemap.xml' 	=> 'sitemap/default/index',
                'sitemap.html' 	=> 'sitemap/default/index/format/html',
			),
		),
        
//		'db'=>array(
//			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
//		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=vs_db',
			'emulatePrepare' => true,
			'username' => 'vs_root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'uploadPath'=>Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'. DIRECTORY_SEPARATOR .'uploads',
        
	),
);