<?php

Yii :: import('zii.widgets.CMenu');

class MainMenu extends CMenu
{
    protected function renderMenuItem($item)
    {
        if(isset($item['url']))
        {
            $label=(isset($item['counterNotifer']) ? ('<span class="notifer">'.$item["counterNotifer"].'</span>') : (' ')) .'<span class="icon"></span> <span class="name">'.$item['label'].'</span>';
                
            return CHtml::link($label,$item['url'],isset($item['linkOptions']) ? $item['linkOptions'] : array());
        }
        else
            return CHtml::tag('span',isset($item['linkOptions']) ? $item['linkOptions'] : array(), $item['label']);
	}
}

