<?php

/**
 * Redactor widget image list action.
 *
 */

class ImageList extends CAction
{
	public $uploadPath;
	public $uploadUrl;

	public function run()
	{
		$name=strtolower($this->getController()->getId());

		if ($this->uploadPath===null) {
			$path=Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads';
			$this->uploadPath=realpath($path);
			if ($this->uploadPath===false) {
				exit;
			}
		}
		if ($this->uploadUrl===null) {
			$this->uploadUrl=Yii::app()->request->baseUrl .'/uploads';
		}

		$attributePath=$this->uploadPath.DIRECTORY_SEPARATOR.$name;
		$attributeUrl=$this->uploadUrl.'/'.$name.'/';

		$files=CFileHelper::findFiles($attributePath, array('fileTypes'=>array('gif','png','jpg','jpeg')));
		$data=array();
		if ($files) {
			foreach($files as $file) {
				$data[]=array(
					'thumb'=>$attributeUrl.basename($file),
					'image'=>$attributeUrl.basename($file),
				);
			}
		}
		echo CJSON::encode($data);
		exit;
	}
}