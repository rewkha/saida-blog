<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $tags
 * @property string $create_time
 * @property integer $author_id
 */
class Post extends CActiveRecord
{
    
    private $_oldTags;
    
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{

		return array(
			array('title, content', 'required'),
			array('title', 'length', 'max'=>128),
			array('tags', 'match', 'pattern'=>'/^[\w\s,а-яА-Я\-]+$/u',
                'message'=>'В тегах можно использовать только буквы.'),
            array('tags', 'normalizeTags'),
			array('id, title, content, tags, create_time, update_time , author_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'author' => array(self::BELONGS_TO, 'Account', 'author_id'),
            'comments' => array(self::HAS_MANY, 'Comment', 'post_id',
                    //'condition'=>'comments.status='.Comment::STATUS_APPROVED,
                    'order'=>'comments.create_time DESC'
                ),
            'commentCount' => array(self::STAT, 'Comment', 'post_id',
                    //'condition'=>'status='.Comment::STATUS_APPROVED
                ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'content' => 'Content',
			'tags' => 'Tags',
			'create_time' => 'Create Time',
			'author_id' => 'Author',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('author_id',$this->author_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    
    public function addComment($comment)
    {
        $comment->post_id=$this->id;
        return $comment->save();
    }
    
    
    /**
     * Normalize tags
     * @param string $attribute
     * @param array $params
     */
    public function normalizeTags($attribute,$params)
    {
        $this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }
    
    
    /**
	 * @return string the URL that shows the detail of the post
	 */
    public function getUrl()
    {
        return Yii::app()->createUrl('blog/view', array(
            'id'=>$this->id,
            'title'=>$this->title,
        ));
    }
    
    /**
	 * @return array a list of links that point to the post list filtered by every tag of this post
	 */
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>$tag));
		return $links;
	}
    
    
    /**
	 * Actions before save.
	 */
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->create_time=$this->update_time=date("Y-m-d H:i:s");
            }
            else
                $this->update_time=date("Y-m-d H:i:s");
            return true;
        }
        else
            return false;
    }
    
    protected function afterSave()
    {
        parent::afterSave();
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags=$this->tags;
    }
    
    protected function afterDelete()
    {
        parent::afterDelete();
        Comment::model()->deleteAll('post_id='.$this->id);
        Tag::model()->updateFrequency($this->tags, '');
    }
}