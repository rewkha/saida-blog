<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $pic
 */
class Account extends CActiveRecord
{
    /**
     * filename
     * @var string
     */
    protected $_file_photo_name;
    
    
    /**
     * Default photo name
     * @var string 
     */
    protected $defaultPhoto='default.jpg';
    
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Account the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('login, name', 'required', 'on'=>'register'),
            array('name', 'required', 'on'=>'edit'),
            array('pic', 'file',
                'allowEmpty'  => true,
                'maxFiles'   => 1,
                'maxSize'   => 1024 * 1024,
                'minSize'   => 0,
                'types'   => 'jpg, png, gif',
                'tooLarge'   => 'Вы загружаете файл слишком большого размера',
                'on'   => array('edit'),
            ),
            array('password', 'required', 'on'=>'changePassword'),
			array('login, name, email, password', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login, name, email, password, pic', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Login',
			'name' => 'Отображаемое имя',
			'email' => 'Email',
			'password' => 'Пароль',
			'pic' => 'Аватар',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('pic',$this->pic,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    /**
     * Validating the password
     * @param type $password
     * @return boolean status
     */
    public function validatePassword($password)
    {
        return hash('sha1', $password)===$this->password;
    }
 
    
    /**
     * Hash the password
     * @param type $password
     * @return string
     */
    public function hashPassword($password)
    {
        return hash('sha1', $password);
    }
    
    /**
	 * Generates a salt that can be used to generate a password hash.
	 *
	 * The {@link http://php.net/manual/en/function.crypt.php PHP `crypt()` built-in function}
	 * requires, for the Blowfish hash algorithm, a salt string in a specific format:
	 *  - "$2a$"
	 *  - a two digit cost parameter
	 *  - "$"
	 *  - 22 characters from the alphabet "./0-9A-Za-z".
	 *
	 * @param int cost parameter for Blowfish hash algorithm
	 * @return string the salt
	 */
	protected function generateSalt($cost=10)
	{
		if(!is_numeric($cost)||$cost<4||$cost>31){
			throw new CException(Yii::t('Cost parameter must be between 4 and 31.'));
		}
		// Get some pseudo-random data from mt_rand().
		$rand='';
		for($i=0;$i<8;++$i)
			$rand.=pack('S',mt_rand(0,0xffff));
		// Add the microtime for a little more entropy.
		$rand.=microtime();
		// Mix the bits cryptographically.
		$rand=sha1($rand,true);
		// Form the prefix that specifies hash algorithm type and cost parameter.
		$salt='$2a$'.str_pad((int)$cost,2,'0',STR_PAD_RIGHT).'$';
		// Append the random salt string in the required base64 format.
		$salt.=strtr(substr(base64_encode($rand),0,22),array('+'=>'.'));
		return $salt;
	}
    
    
    /**
     * Загружает изображение
     *
     * @return string $_img_name
     */
    public function uploadFilePhoto()
    {
        //Загружаем фото
        $this->pic = CUploadedFile::getInstance($this, 'pic');
        if (empty($this->pic) == false) {
            //Формируем имя для фотографии
            $this->_file_photo_name = uniqid() . '.jpg';

            //Свойство _file_photo_name хранит текущее имя загруженного/загружаемого файла
            //$this -> _file_photo_name
            //Формируем путь к каталогу (с названием файла) где будут хранится фотографии пользователя
            $normal =Yii::app()->basePath."/../public/images/avatar/" . $this->_file_photo_name;
            //Теперь, переменная $normal, может содержать следующее "./files/upload/users/normal/1292056803yOHri.jpg"
            //Сохраняем файл
            $this->pic->saveAs($normal);

            //Подключаем расширение image, используемое для ресайза изображения
            Yii::import('application.extensions.image.Image');

            //Делаем ресайз только что загруженному изображению
            $Image = Image::factory($normal);
             
            
            $Image->centeredpreview(70, 70);
            $Image->save($normal);

        }
        return $this->_file_photo_name;
    }
    
    /**
     * Удаляем изображения пользователей
     *
     * @param string $file_name имя удаляемого файла
     */
    public function deleteFilePhoto($file_name = false) 
    {
        //В том случае, если данные не переданы, берем свойство модели
        if ($file_name === false) {
            $file_name = $this->_file_photo_name;
        }
        if ((file_exists(Yii::app()->basePath."/../public/images/avatar/".$file_name)) && ($file_name != '')) 
            unlink(Yii::app()->basePath."/../public/images/avatar/".$file_name);
    }
    
    public function getAvatar() 
    {
        if ($this->pic && $this->pic != '' && file_exists(Yii::app()->basePath."/../public/images/avatar/".$this->pic)) {
            return $this->pic;
        }
        return $this->defaultPhoto;
    }
}