<?php

class m130111_191708_table_init extends CDbMigration
{
	public function up()
	{
            $this->createTable('account', array(
                    'id' => 'pk',
                    'login' => 'varchar(255) NOT NULL',
                    'name' => 'varchar(255) NOT NULL',
                    'email' => 'varchar(255) NOT NULL DEFAULT ""',
                    'password' => 'varchar(255) NOT NULL DEFAULT "0"',
                    'pic' => 'varchar(255) NOT NULL DEFAULT "" COMMENT "URL аватарки"',
                ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Аккаунты"');
            
            $this->createTable('post', array(
                    'id' => 'pk',
                    'title' => 'varchar(128) NOT NULL',
                    'content' => 'TEXT NOT NULL',
                    'tags' => 'TEXT',
                    'create_time' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
                    'update_time' => 'timestamp',
                    'author_id' => 'int(11) NOT NULL DEFAULT "0"',
                ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Посты в блог"');
            
            $this->createTable('tag', array(
                    'id' => 'pk',
                    'name' => 'varchar(128) NOT NULL',
                    'frequency' => 'int(11) NOT NULL DEFAULT 1',
                ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Теги в блог"');
            
            $this->createTable('comment', array(
                    'id' => 'pk',
                    'content' => 'TEXT NOT NULL',
                    'author' => 'varchar(128) NOT NULL',
                    'email' => 'varchar(255) NOT NULL DEFAULT ""',
                    'post_id' => 'int(11) NOT NULL',
                    'create_time' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
                ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Комменты в блог"');
            
            $this->createTable('static_pages', array(
                    'id' => 'pk',
                    'name' => 'varchar(128) NOT NULL',
                    'content' => 'TEXT',
                    'type'  =>  'int(2) NOT NULL DEFAULT "1"'
                ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Статические страницы"');
            
            $this->createIndex('fk_post_author', 'post', 'author_id');
            $this->createIndex('fk_comment_post', 'comment', 'post_id');
            
            
	}

	public function down()
	{
            $this->dropTable('account');
            $this->dropTable('post');
            $this->dropTable('tag');
            $this->dropTable('comment');
            $this->dropTable('static_pages');
            
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
        
	*/
}