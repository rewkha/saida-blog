<?php

class m130129_234653_add_gallery extends CDbMigration
{
	public function up()
	{
        $this->createTable('gallery', array(
                'id' => 'pk',
                'versions_data' => 'TEXT NOT NULL',
                'name' => 'tinyint(1) NOT NULL',
                'description' => 'tinyint(1) NOT NULL',
                'gallery_name' => 'varchar(512) NOT NULL',
                'gallery_desc' => 'TEXT DEFAULT NULL',
                'title_photo_id' => 'int(11) NOT NULL',

            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT="Галереи"');

        $this->createTable('gallery_photo', array(
                'id' => 'pk',
                'gallery_id' => 'int(11) NOT NULL',
                'rank' => 'int(11) DEFAULT 0',
                'name' => 'varchar(512) NOT NULL',
                'description' => 'TEXT DEFAULT NULL',
                'file_name' => 'varchar(128) NOT NULL',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        
	}

	public function down()
	{
		$this->dropTable('gallery');
        $this->dropTable('gallery_photo');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}