<?php

class m130112_214104_insert_data extends CDbMigration
{
	public function up()
	{
            $this->insert(
            'account',
            array(
                'id' => 1,
                'login' => 'saida',
                'name' =>  'Саида',
                'email' => 'saida@example.com',
                'password' => '$2a$10$ZSejo1X/GfxHa57gF7zSauKX8W1eBeFJC9DJPTFyCb5c1XvFcLfZO',
                'pic' => '',
            ));
            
            $this->insert(
            'account',
            array(
                'id' => 2,
                'login' => 'vicoolya',
                'name' =>  'Виктория',
                'email' => 'vika@example.com',
                'password' => '$2a$10$TnHgtShcOSzbGoi9JT1P9uW5dxThVfJlrIlGnk2ZobNmPB6JESzrC',
                'pic' => '',
            ));
            
            $this->insert(
            'account',
            array(
                'id' => 3,
                'login' => 'vicoolya and saida',
                'name' =>  'Виктория и Саи Да',
                'email' => '0',
                'password' => '0',
                'pic' => '',
            ));
            
            $this->insert(
            'static_pages',
            array(
                'id' => 1,
                'name' => 'Добро пожаловать',
                'content' => '',
                'type'  =>  1,
            ));
            
            $this->insert(
            'static_pages',
            array(
                'id' => 2,
                'name' => 'Контакты',
                'content' => '',
                'type'  =>  2,
            ));
            
            $this->insert(
            'static_pages',
            array(
                'id' => 3,
                'name' => 'Про нас',
                'content' => '',
                'type'  =>  3,
            ));
            $this->insert(
            'tag',
            array(
                'id' => 2,
                'name' => 'photo',
            ));
            
	}

	public function down()
	{
            $this->execute('DELETE FROM `account` WHERE id IN (1,2)');                
            $this->execute('DELETE FROM `static_pages` WHERE id IN (1,2)');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}