<?php

class AdminController extends Controller
{
    
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';
    
    
    /**
     * Assign filters to current controller.
     * This method is a part of Yii Framework API.
     * For more information look at yiiframework.com
     *
     * @link    http://www.yiiframework.com/doc/api/1.1/CController#filters-detail
     * @return  array  Array of filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    /**
     * Declares actions access rules.
     * Returns the access rules for this controller. Override this method if you use the accessControl filter.
     *
     * @return  array  list of access rules. See CAccessControlFilter for details about rule specification.
     * @see     CController::accessRules()
     * @link    http://www.yiiframework.com/doc/api/1.1/CController#accessRules-detail
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' =>array (
                    'login',
                ),
                'users'=>array('*'),
            ),
            array(
                'allow',
                'actions' =>array (
                    'index', 'logout', 'posts', 'profile','galleries', 'newGallery', 
                    'editGallery', 'changePassword', 'editPages', 'deleteAvatar',
                    'fileUpload', 'imageUpload', 'imageList', 'commonPhoto'
                ),
                'users'=>array('@'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );
    }
   
    
    public function beforeRender($view) 
    {
        $result = parent::beforeRender($view);
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->baseUrl
                . '/css/admin.css'
        );
        Yii::app()->clientScript->registerCoreScript('jquery');
        $this->registerClientTwitterBootstrap();
        return $result;
    }
    
    
    /**
     * Image and file upload with default actions of redactor
     * @return void
     */
    public function actions()
    {
        return array(
            'fileUpload'=>array(
                'class'=>'application.widgets.redactorjs.actions.FileUpload',
                'uploadCreate'=>true,
                'uploadPath' => Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'. DIRECTORY_SEPARATOR .'uploads',
             ),
            'imageUpload'=>array(
                'class'=>'application.widgets.redactorjs.actions.ImageUpload',
                'uploadCreate' => true,
                'uploadPath' => Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'. DIRECTORY_SEPARATOR .'uploads',
             ),
            'imageList'=>'application.widgets.redactorjs.actions.ImageList',
            'uploadPath' => Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'. DIRECTORY_SEPARATOR .'uploads',
        );
    }
    
    
    /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
    
    
    /**
     * Logs out the current user
     * and redirect to homepage.
     * @return void
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    
    public function actionIndex()
	{
        $this->render('index');
	}
    
    /**
	 * Manages all posts.
	 */
	public function actionPosts()
	{
		$model=new Post('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];

		$this->render('posts',array(
			'model'=>$model,
		));
	}
    
    
    /**
	 * Edit profile.
	 */
    public function actionProfile()
    {
        if (! $model=Account::model()->findByPk(Yii::app()->user->id)) {
            return 'error user'; die;
        }
        $model->setScenario('edit');
        $oldfile = $model->pic;
        $account = Yii::app()->request->getParam('Account');
        
        if($account) {
            $model->attributes=$account;
            if($model->validate()) {   
                try {
                    if ($model->pic = $model->uploadFilePhoto()) {
                        $model->save(false, array('pic','name','email'));
                        $model->deleteFilePhoto($oldfile);
                    } else {
                        $model->save(false, array('name','email'));
                        $model->pic = $oldfile;
                    }
                }
                catch (Exception $e) {
                    $model -> deleteFilePhoto();
                    $model -> addError('*','Ошибка при загрузке изображения.');
                }
            }
        }
        $this->render('profile',array('model'=>$model));
    }
    
    /**
	 * Edit double photo.
	 */
    public function actionCommonPhoto()
    {
        if (! $model=Account::model()->findByPk(3)) {
            return 'error user'; die;
        }
        $model->setScenario('common');
        $oldfile = $model->pic;
        $account = Yii::app()->request->getParam('Account');
        
        if($account) {
            $model->pic=$account['pic'];       
            try {
                if ($model->pic = $model->uploadFilePhoto()) {
                    $model->save(false, array('pic','name','email'));
                    $model->deleteFilePhoto($oldfile);
                } else {
                    $model->save(false, array('name','email'));
                    $model->pic = $oldfile;
                }
            }
            catch (Exception $e) {
                $model -> deleteFilePhoto();
                $model -> addError('*','Ошибка при загрузке изображения.');
            }
        }
        $this->render('common_pic',array('model'=>$model));
    }
    
    /**
	 * Edit password.
	 */
    public function actionDeleteAvatar()
    {
        $model=Account::model()->findByPk(Yii::app()->user->id);
        $model->deleteFilePhoto($model->pic);
        $model->pic = '';
        $model->save(false, array('pic'));
        $this->redirect(Yii::app()->createUrl('admin/profile'));
    }
    
    /**
	 * Edit password.
	 */
    public function actionChangePassword()
    {
        $model=Account::model()->findByPk(Yii::app()->user->id);
        $model->setScenario('changePassword');
        if(isset($_POST['Account']))
        {
            $model->attributes=$_POST['Account'];
            if($model->validate())
            {
                $model->password = $model->hashPassword($model->password);
                $model->save(false, array('password'));
                Yii::app()->user->setFlash('success', "Пароль успешно изменен.");
            }
        }
        $this->render('change_password',array('model'=>$model));
    }
    
    
    /**
	 * Edit pages.
	 */
    public function actionEditPages()
    {
        $type = Yii::app()->request->getParam('type');
        if ((! $type) || (! in_array($type, array(1,2,3) ) )) {
            $type = 1;
        }
        
        if ($model = StaticPages::model()->find('type = :type', array(':type'=>$type))) {
            if(isset($_POST['StaticPages'])) {
                $model->attributes=$_POST['StaticPages'];
                $model->save(); 
            }
            $this->render('edit_pages',array('model'=>$model, 'current'=>$type));  
        }
    }
    
    
    /**
	 * Edit galleries.
	 */
    public function actionGalleries()
    {
        $galleries = Gallery::model()->findAll();
        $this->render('gallery',array('galleries'=>$galleries));
    }
    
    /**
	 * Edit galleries.
	 */
    public function actionNewGallery()
    {
        $gallery = new Gallery();
        $gallery->name = true;
        $gallery->description = true;
        $gallery->versions = array(
            'normal' => array(
                'resize' => array(1280, 1280),
            ),
        );
        $gallery->save();
        $this->redirect(Yii::app()->createUrl('admin/editGallery', array('id'=>$gallery->id)));
    }
    
    /**
	 * Edit galleries.
	 */
    public function actionEditGallery()
    {
        $id = Yii::app()->request->getParam('id');
        if ($id && $gallery = Gallery::model()->findByPk($id)) {
            $postData = Yii::app()->request->getParam('Gallery');
            if ($postData) {
                $gallery->attributes = $postData;
                $gallery->save();
            }
            
            $this->render('edit_gallery',array('gallery'=>$gallery));
        } else {
            Yii::app()->user->setFlash('error', "Галереи с id№". $id . " не существует");
            $this->redirect(Yii::app()->createUrl('admin/galleries'));
        }
    }
    
    /**
     * Register twitter bootstrap css framework
     * that realize UI components and CSS styles.
     *
     * @return Controller This object
     * @access private
     */
    private function registerClientTwitterBootstrap()
    {
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->baseUrl
                . '/vendors/bootstrap/css/bootstrap.min.css'
        );
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->baseUrl
                . '/vendors/bootstrap/js/bootstrap.min.js'
        );
        return $this;
    }
}