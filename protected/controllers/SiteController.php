<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
//			'page'=>array(
//				'class'=>'CViewAction',
//			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $connection=Yii::app()->db; // так можно делать, если в конфигурации настроен компонент соединения "db"
        $sql='SELECT * FROM `static_pages` WHERE type=1';
        $content=$connection->createCommand($sql)->queryAll();
        if (empty($content[0])){
            $title = 'Welcome';
            $page = '';
        } else {
            $title = $content[0]['name'];
            $page = $content[0]['content'];
        }
		$this->render('index', array('content'=>$page, 'title'=>$title));
	}

    /**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionAbout()
	{
		$connection=Yii::app()->db; // так можно делать, если в конфигурации настроен компонент соединения "db"
        $sql='SELECT * FROM `static_pages` WHERE type=3';
        $content=$connection->createCommand($sql)->queryAll();
        if (empty($content[0])){
            $title = 'About Us';
            $page = '';
        } else {
            $title = $content[0]['name'];
            $page = $content[0]['content'];
        }
        $this->render('pages/about', array('content'=>$page, 'title'=>$title));
	}
    
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{   
            
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$connection=Yii::app()->db; // так можно делать, если в конфигурации настроен компонент соединения "db"
        $sql='SELECT * FROM `static_pages` WHERE type=2';
        $content=$connection->createCommand($sql)->queryAll();
        if (empty($content[0])){
            $title = 'About Us';
            $email = Yii::app()->params['adminEmail'];
        } else {
            $title = $content[0]['name'];
            $email = $content[0]['content'];
        }
        
        $model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail($email,$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model, 'title'=>$title));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
    
}