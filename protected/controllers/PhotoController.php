<?php

class PhotoController extends Controller
{
    /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','gallery'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex()
	{
        $galleries = Gallery::model()->findAll();
        
        $this->render('index', array('galleries'=>$galleries));
	}
    
    public function actionGalleries()
	{
        $galleries = Gallery::model()->findAll();
        
        $this->render('index', array('galleries'=>$galleries));
	}
    
    public function actionGallery()
	{
        $this->registerGalleriffic();
        $id = Yii::app()->request->getParam('id');
        $gallery = Gallery::model()->findByPk($id);
        $photos = $gallery->galleryPhotos;
        
        $this->render('view', array('photos'=>$photos));
	}

    
    private function registerGalleriffic()
    {
        Yii::app()->getClientScript()->registerCoreScript('jquery');
        
//        Yii::app()->clientScript->registerCssFile(
//            Yii::app()->baseUrl
//                . '/vendors/galleriffic/css/basic.css'
//        );
//        Yii::app()->clientScript->registerCssFile(
//            Yii::app()->baseUrl
//                . '/vendors/galleriffic/css/black.css'
//        );
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->baseUrl
                . '/vendors/galleriffic/js/jquery.galleriffic.js',
            CClientScript::POS_HEAD
        );
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->baseUrl
                . '/vendors/galleriffic/js/jquery.history.js',
            CClientScript::POS_HEAD
        );
        Yii::app()->clientScript->registerScriptFile(
            Yii::app()->baseUrl
                . '/vendors/galleriffic/js/jquery.opacityrollover.js',
            CClientScript::POS_HEAD
        );
        Yii::app()->clientScript->registerCssFile(
            Yii::app()->baseUrl
                . '/css/gallery.css'
        );
        return $this;
    }
    
    
}